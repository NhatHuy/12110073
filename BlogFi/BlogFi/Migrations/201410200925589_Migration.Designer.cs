// <auto-generated />
namespace BlogFi.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    public sealed partial class Migration : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Migration));
        
        string IMigrationMetadata.Id
        {
            get { return "201410200925589_Migration"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
