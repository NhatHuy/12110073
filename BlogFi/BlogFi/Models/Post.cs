﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BlogFi.Models
{
    public class Post
    {
        public int ID { set; get; }
        [StringLength(500, ErrorMessage = ("Title co do dai tu 20 den 500 ky tu"), MinimumLength = 20)]
        public string Title { set; get; }
        [MinLength(50, ErrorMessage = ("Noi dung phai co it nhat 50 ky tu"))]
        public string Body { set; get; }
        public DateTime DateCreated { set; get; }
        public DateTime DataUpdate { set; get; }

        //1 post co nhieu comment (Colection comment)
        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }

        //post thuoc account nao
        public int AccountID { set; get; }
        //1 post chi thuoc 1 account
        public virtual Account Accounts { set; get; }
    }
}