﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BlogFi.Models
{
    public class Account
    {
        public int ID { set; get; }
        [DataType(DataType.EmailAddress)]
        public string Email { set; get; }
        [DataType(DataType.Password)]
        public string Password { set; get; }
        [StringLength(100, ErrorMessage = ("FristName phai nam trong khoan 10 den 100 ky tu"), MinimumLength = 10)]
        public string FirstName { set; get; }
        [StringLength(100, ErrorMessage = ("LastName phai nam trong khoan 10 den 100 ky tu"), MinimumLength = 10)]
        public string LastName { set; get; }

        //1 account co nhieu post (Colection post)
        public virtual ICollection<Post> Posts { set; get; }
    }
}