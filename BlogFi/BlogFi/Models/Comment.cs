﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BlogFi.Models
{
    public class Comment
    {
        [Required]
        public int ID { set; get; }
        [MinLength(50, ErrorMessage = ("Noi dung phai co it nhat 50 ky tu"))]
        public string Body { set; get; }
        public DateTime DateCreated { set; get; }
        public DateTime DataUpdate { set; get; }
        public string Author { set; get; }

        //chi ro ra comment do thuoc post nao
        public int PostID { set; get; }
        //1 comment chi thuoc 1 post
        public virtual Post Posts { set; get; }
    }
}