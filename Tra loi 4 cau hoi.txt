﻿1. Lợi ích mà website mang lại
2. Sư khác biệt so với các website cùng loại (nêu tên và ghi lại sự khác biệt)
3. Nếu không dùng website của bạn thì người dùng bị mất gì?
4. Website của bạn là Vitamin hay PainKiller?


1. Giúp người dùng chiến đấu với chính bản thân mình, so sánh mình của hôm nay với
 hôm qua và biết rằng hôm nay mình phải làm được gì đó tốt hơn hôm qua. Thay đổi chính
bản thân mình khiến cuộc sống trở nên thú vị hơn.
2. Em không biết có website nào có cùng loại với mình hay không nhưng điểm khác biệt em nghĩ là 
lớn nhất so với các website khác đó là t riêng tư tuyệt đối, trong trang web người dùng sẽ không cần
quan tâm là những thử thách do mình đặt ra cho chính mình nó có quá nhỏ hoặc quá lớn hay không, có sợ 
bị người khác chê cười hay không, điều quan trọng nhất mà người dùng nhận được là vượt qua được chính
bản thân mình.
3. Nếu không dùng trang web này: Những người có tính cách hay hẹn lại, không biết đặt kế hoạch sẽ không
biết được ngày nay mình làm được những gì để rồi đến cuối ngày không biết là ngày hôm nay mình đã mang
lại những gì cho bản thân, tính cách như vậy lâu dần sẽ trở thành thói quen khó sửa và sẽ ảnh hưởng đến
 tương lai sau này.
4. Về loại trang web thì là Vitamin hay PainKiller thì hôm bữa thầy nói em không hiểu về bản chất của 2 
loại trang web này lắm, em lên mạng tìm đọc cũng không thấy tài liệu nói về cái này, mong thầy khái quát
lại giúp em, em cảm ơn thầy.